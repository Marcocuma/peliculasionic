import {Component} from '@angular/core';
import {LoginService} from './services/login.service';
import {ActionSheetController} from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent{
  componentSubscription;
  constructor(public loginService: LoginService) {}

}
