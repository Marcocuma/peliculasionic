import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicStorageModule } from '@ionic/storage-angular';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {PersonalmoviesService} from './services/personalmovies.service';
import {LoginService} from './services/login.service';
import {Storage} from '@ionic/storage';
import {LoginPageModule} from './login/login.module';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import {UiService} from './services/ui.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, HttpClientModule, IonicModule.forRoot(), IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AppRoutingModule, LoginPageModule, TranslateModule.forRoot({
      defaultLanguage: 'es',
      loader: {provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]}
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, HttpClient, Storage,
    PersonalmoviesService, LoginService, UiService],
  bootstrap: [AppComponent],
})
export class AppModule {}
