import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {LoginService} from '../services/login.service';
import {ActionSheetController, AlertController, IonInput} from '@ionic/angular';
import {UiService} from '../services/ui.service';
import {TranslateService} from '@ngx-translate/core';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email = '';
  password = '';
  @ViewChild('passInput') passInput: IonInput;
  constructor(public loginService: LoginService, public alertController: AlertController,
              public actionSheetController: ActionSheetController, public uiService: UiService,
              public translate: TranslateService) {
    this.translate.setDefaultLang('en');
  }

  ngOnInit() {
  }
  async anonimousLogin() {
    // Pone la variable login a false, lo que indicará en el servicio de la lista 'personalMovies'
    // posteriormente donde va a guardar la lista y dejara entrar al usuario a la pagina
    // Pregunto si quiere eliminar la pelicula
    const keys: string[] = ['ANONIMOUSLOGIN', 'ANONIMOUSLOGINMESSAGE', 'NO', 'YES'];
    this.translate.get(keys).pipe(take(1)).subscribe(async value => {
      const alert = await this.alertController.create({
        header: value[keys[0]],
        message: value[keys[1]],
        buttons: [
          {
            text: value[keys[2]],
            role: 'cancel',
            cssClass: 'secondary'
          }, {
            text: value[keys[3]],
            handler: () => {
              this.loginService.anonimousLogin();
            }
          }
        ]
      });
      alert.present();
    });
  }
  emailKeyPress(code){
    if (code === 13){
      this.passInput.setFocus();
    }
  }
  passKeyPress(code){
    if (code === 13){
      this.login();
    }
  }
  login(){
    if (this.email.length > 0 && this.password.length > 0) {
      this.loginService.login(this.email, this.password);
    }
  }
  register(){
    if (this.email.length > 0 && this.password.length > 0) {
      this.loginService.register(this.email, this.password);
    }
  }
}
