export class Movie {
  id: string;
  title: string;
  year: string;
  poster: string;

  constructor(id: string, title: string, year: string, poster: string) {
    this.title = title;
    this.year = year;
    this.poster = poster;
    this.id = id;
  }
}
