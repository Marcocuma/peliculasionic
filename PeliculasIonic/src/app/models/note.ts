export class Note {
  value: string;
  // 1 baja, 2 media, 3 alta
  priority: number;

  constructor(value: string, priority: number) {
    this.value = value;
    this.priority = priority;
  }
}
