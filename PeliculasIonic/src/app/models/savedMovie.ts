
import {Movie} from './movie';

export class SavedMovie {
  movie: Movie;
  priority: number;
  seen: boolean;

  constructor(movie: Movie, priority: number, seen: boolean) {
    this.movie = movie;
    this.priority = priority;
    this.seen = seen;
  }
}
