import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { NotesPageRoutingModule } from './saved-routing.module';

import { SavedPage } from './saved.page';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NotesPageRoutingModule,
        TranslateModule
    ],
  declarations: [SavedPage]
})
export class NotasPageModule {}
