import {Component, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {PersonalmoviesService} from '../services/personalmovies.service';
import {SavedMovie} from '../models/savedMovie';
import {ActionSheetController, AlertController, IonButton, IonInput, IonSegment, ToastController} from '@ionic/angular';
import {Movie} from '../models/movie';
import {NavigationExtras, Router} from '@angular/router';
import {take} from 'rxjs/operators';
import {LoginService} from '../services/login.service';
import {AnimationController} from '@ionic/angular';
import {UiService} from '../services/ui.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-notas',
  templateUrl: './saved.page.html',
  styleUrls: ['./saved.page.scss']
})
export class SavedPage implements OnInit {
  value = '';
  isFiltered = false;
  seenFilter = 'all';
  showFilters = true;
  movies: [string, SavedMovie][];
  moviesAux: [string, SavedMovie][];
  loaded = false;
  @ViewChild(IonInput) input: IonInput;
  @ViewChild('addFilterButton') search: IonButton;
  @ViewChild('eraseFilterButton') clean: IonButton;
  @ViewChild('segmentSeen') segment: IonSegment;
  constructor(public personalMovies: PersonalmoviesService, public router: Router,
              public alertController: AlertController, public loginService: LoginService,
              public animationControler: AnimationController, public actionSheetController: ActionSheetController,
              public uiService: UiService, public translate: TranslateService) {  }
  ngOnInit() {

  }
  ionViewDidEnter(){
    this.loaded = false;
    this.personalMovies.getMovies().pipe(take(2)).subscribe((value1 => {
      // Transformo el mapa en un array de tuplas para poder acceder
      // al valor y clave desde el ngFor, y despues lo ordeno
      this.movies = Array.from(value1.entries());
      this.sortMovies();
      this.loaded = true;
    }));
  }
  ionViewWillLeave(){
    this.eraseFilter();
    this.saveBD();
  }

  trackItems(index: number, item: [string, SavedMovie]){
    // Esta funcion evita que se actualice la lista entera cada vez que cambia un item
    // Devuelve el primer valor de la tupla ya que es único por pelicula
    return item[0];
  }
  onEnterFilter(code){
    if (code === 13){
      this.addFilter();
    }
  }
  async askForRemove(index: number){
    // Pregunto si quiere eliminar la pelicula
    const keys: string[] = ['DELETE', 'DELETEQUESTION', 'CANCEL', 'DELETE'];
    this.translate.get(keys).pipe(take(1)).subscribe(async value => {
      const alert = await this.alertController.create({
        header: value[keys[0]],
        message: value[keys[1]],
        buttons: [
          {
            text: value[keys[2]],
            role: 'cancel',
            cssClass: 'secondary'
          }, {
            text: value[keys[3]],
            handler: () => {
              this.removeMovie(index);
            }
          }
        ]
      });
      alert.present();
    });
  }
  showCardBody(id){
    // Si esta oculto, elimino la propieda hidden y hago la animacion de opacidad
    // Si no esta oculto, hago la animacion y despues le pongo la propiedad hidden
    if (document.querySelector('#' + id).getAttribute('hidden') === 'true'){
      document.querySelector('#' + id).removeAttribute('hidden');
      this.animationControler.create()
        .addElement(document.querySelector('#' + id))
        .duration(250)
        .iterations(1)
        .fromTo('opacity', '0', '1')
        .play();
    } else {
      this.animationControler.create()
        .addElement(document.querySelector('#' + id))
        .duration(250)
        .iterations(1)
        .fromTo('opacity', '1', '0')
        .play().then(value1 => {
          document.querySelector('#' + id).setAttribute('hidden', 'true');
        });
    }
  }
  public removeMovie(index: number){
    // Elimina la pelicula del Array
    this.movies.splice(index, 1);
    this.sortMovies();
    this.uiService.setToast('MOVIEDELETED');
  }
  public changePriority(index: number, event){
    // Cambia la priorida de la pelicula
    const tuple: [string, SavedMovie] = this.movies[index];
    tuple[1].priority = event.target.value;
    this.movies[index] = tuple;
    this.sortMovies();
  }
  public addFilter(){
    // Activo el filtro y ordeno el array
    this.input.disabled = true;
    this.search.disabled = true;
    this.isFiltered = true;
    this.clean.disabled = false;
    this.moviesAux = this.movies;
    this.segment.disabled = true;
    this.sortMovies();
  }
  public eraseFilter(){
    // Desactivo el filtro, limpio el campo del titulo y recargo la lista desde el servicio
    if (this.isFiltered) {
      this.value = '';
      this.isFiltered = false;
      this.input.disabled = false;
      this.search.disabled = false;
      this.clean.disabled = true;
      this.segment.disabled = false;
      this.seenFilter = 'all';
      for (const item of this.movies) {
        const index: number = this.moviesAux.indexOf(item);
        if (index > -1) {
          this.moviesAux[index] = item;
        }
      }
      this.movies = this.moviesAux;
      this.sortMovies();
    }
  }
  public sortMovies(){
    // Utilizo una variable auxiliar para que no actualice la vista por cada cambio
    // Si esta activado el filtro, primero obtengo un array con las tuplas que contienen las pelicula con el nombre introducido
    let moviesAux = this.movies;
    if (this.isFiltered ) {
      if (this.value.length > 0) {
        moviesAux = moviesAux.filter((a) => {
          if (a[1].movie.title.toLowerCase().includes(this.value.toLowerCase())) {
            return a;
          }
        });
      }
      switch (this.seenFilter) {
        case 'seen': {
          moviesAux = moviesAux.filter((a) => {
            if (a[1].seen) {
              return a;
            }
          });
        }            break;
        case 'notseen': {
          moviesAux = moviesAux.filter((a) => {
            if (!a[1].seen) {
              return a;
            }
          });
        }               break;
      }
    }
    // Ordeno por prioridad
    this.movies = moviesAux.sort((a, b) => {
      if (a[1].priority > b[1].priority){
        return -1;
      } else {
        return 1;
      }
    });
  }
  public toggleFilter(){
    if (!this.showFilters) {
      this.animationControler.create()
        .addElement(document.querySelector('#containerFilters'))
        .duration(250)
        .iterations(1)
        .fromTo('opacity', '1', '0')
        .play().then(value1 => {
          this.showFilters = !this.showFilters;
        });
    } else {
      this.showFilters = !this.showFilters;
      this.animationControler.create()
        .addElement(document.querySelector('#containerFilters'))
        .duration(250)
        .iterations(1)
        .fromTo('opacity', '0', '1')
        .play();
    }
  }
  public seeMore(movie: Movie){
    const navigationExtras: NavigationExtras = {
      state: {
        id: movie.id
      }
    };
    this.router.navigate(['tabs/detail'], navigationExtras);
  }
  public buttonSave(){
    this.saveBD();
    this.uiService.setToast('SAVEDCORRECTLY');
    this.sortMovies();
  }
  public saveBD(){
    if (this.loaded) {
      if (this.isFiltered) {
        for (const item of this.movies) {
          const index: number = this.moviesAux.indexOf(item);
          if (index > -1) {
            this.moviesAux[index] = item;
          }
        }
        this.movies = this.moviesAux;
      }
      this.personalMovies.setMovies(new Map<string, SavedMovie>(this.movies));
    }
  }
  async logout() {
    const keys: string[] = ['LOGOUT', 'LOGOUTQUESTION', 'NO', 'YES'];
    this.translate.get(keys).pipe(take(1)).subscribe(async value => {
      const alert = await this.alertController.create({
        header: value[keys[0]],
        message: value[keys[1]],
        buttons: [
          {
            text: value[keys[2]],
            role: 'cancel',
            cssClass: 'secondary'
          }, {
            text: value[keys[3]],
            handler: () => {
              this.loginService.logout();
            }
          }
        ]
      });
      alert.present();
    });
  }
}
