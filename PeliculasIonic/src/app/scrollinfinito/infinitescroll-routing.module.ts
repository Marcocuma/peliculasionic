import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfinitescrollComponent } from './infinitescroll.component';

const routes: Routes = [
  {
    path: '',
    component: InfinitescrollComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfinitescrollRoutingModule {}
