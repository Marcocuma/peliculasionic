import {Component, OnInit, ViewChild} from '@angular/core';
import {Movie} from '../models/movie';
import {MovieServiceService} from '../services/movie-service.service';
import {NavigationExtras, Router} from '@angular/router';
import {
  ActionSheetController,
  AlertController,
  IonContent,
  IonFab,
  IonFabButton,
  IonInfiniteScroll,
  IonVirtualScroll,
  ToastController
} from '@ionic/angular';
import {PersonalmoviesService} from '../services/personalmovies.service';
import {SavedMovie} from '../models/savedMovie';
import {LoginService} from '../services/login.service';
import {UiService} from '../services/ui.service';
import {TranslateService} from '@ngx-translate/core';
import {take} from 'rxjs/operators';


@Component({
  selector: 'app-scrollinfinito',
  templateUrl: './infinitescroll.component.html',
  styleUrls: ['./infinitescroll.component.scss'],
})
export class InfinitescrollComponent implements OnInit {
  title = '';
  searchingTitle = '';
  loading: boolean;
  firstLoad = false;
  movieList: Movie[];
  poster = false;
  scrollBlock = false;
  page;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild('scrollTopFab') fab: IonFab;
  @ViewChild(IonContent) content: IonContent;
  @ViewChild(IonVirtualScroll) virscroll: IonVirtualScroll;
  constructor(public peliculaService: MovieServiceService, public personalMovie: PersonalmoviesService, public router: Router,
              public loginService: LoginService,
              public actionSheetController: ActionSheetController, public alertController: AlertController,
              public uiService: UiService, public translate: TranslateService) { }
  ngOnInit() {
  }

  onEnterSearch(code){
    if (code === 13){
      this.startScroll();
    }
  }
  public checkScroll(event){
    if (event.detail.scrollTop > 100){
      this.fab.activated = false;
    } else {
      this.fab.activated = true;
    }
  }
  public moveScroll(){
    if (!this.scrollBlock) {
      this.scrollBlock = true;
      if (this.fab.activated) {
        this.content.scrollToBottom(1500).then(value => {
          this.scrollBlock = false;
        });
      } else {
        this.content.scrollToTop(1500).then(value => {
          this.scrollBlock = false;
        });
      }
    }
  }
  public changeView(event){
    if ( event.target.value === 'list'){
      this.poster = false;
    } else {
      this.poster = true;
    }
  }
  async actionModal(movie: Movie) {
    const keys: string[] = ['ADD', 'DETAILS', 'CANCEL'];
    this.translate.get(keys).pipe(take(1)).subscribe(async value => {
      const actionSheet = await this.actionSheetController.create({
        header: movie.title,
        cssClass: 'my-custom-class',
        buttons: [{
          text: value[keys[0]],
          icon: 'add-circle-outline',
          handler: () => {
            if (this.personalMovie.exist(movie.id)) {
              this.uiService.setToast('MOVIEINTHELIST');
            } else {
              this.seeLater(movie);
            }
          }
        }, {
          text: value[keys[1]],
          icon: 'information-circle',
          handler: () => {
            this.seeMore(movie);
          }
        }, {
          text: value[keys[2]],
          icon: 'close',
          role: 'cancel',
          handler: () => {

          }
        }]
      });
      await actionSheet.present();
    });
  }

  public startScroll(){
    this.movieList = [];
    this.firstLoad = true;
    this.page = 1;
    this.infiniteScroll.disabled = false;
    this.loading = false;
    // Hago la primera carga
    if (!this.loading && this.title.length > 0) {
      this.searchingTitle = this.title;
      this.loading = true;
      this.peliculaService.getPagedMovies(this.searchingTitle, this.page).subscribe(result => {
        const field = 'Search';
        if (result[field] != null) {
          result[field].forEach(element => {
            this.movieList.push(new Movie(element.imdbID, element.Title, element.Year, element.Poster));
          });
        } else {
          this.infiniteScroll.disabled = true;
          this.uiService.setToast('SEARCHNORESULT');
        }
        this.page++;
        this.loading = false;
        this.firstLoad = false;
      });
    } else {
      this.loading = false;
      this.firstLoad = false;
      this.infiniteScroll.disabled = true;
      this.movieList.length = 0;
    }
  }
  public loadData(event){
    // Controlo que no se esten cargando otros datos para que no se solape
    if (!this.loading) {
      this.loading = true;
      this.peliculaService.getPagedMovies(this.searchingTitle, this.page).subscribe(result => {
        const field = 'Search';
        // Si no encuentra valores, desactiva el scroll infinito
        if (result[field] != null) {
          result[field].forEach(element => {
            this.movieList.push(new Movie(element.imdbID, element.Title, element.Year, element.Poster));
          });
          if (!this.poster){
            this.virscroll.checkEnd();
          }
        } else {
          this.infiniteScroll.disabled = true;
          this.uiService.setToast('NOMORERESULTS');
        }
        // Cuando termina la carga, aumento la pagina y completo el evento
        this.page++;
        this.loading = false;
        event.target.complete();
      });
    }
  }
  public seeMore(movie: Movie){
    const navigationExtras: NavigationExtras = {
      state: {
        id: movie.id
      }
    };
    this.router.navigate(['tabs/detail'], navigationExtras);
  }
  public seeLater(movie: Movie){
    if (!this.personalMovie.exist(movie.id)){
      const movieSeeLater = new SavedMovie(movie, 1, false);
      this.personalMovie.addMovie(movieSeeLater, movie.id);
      this.uiService.setToast('MOVIEADDED');
    }
  }
  async logout() {
    const keys: string[] = ['LOGOUT', 'LOGOUTQUESTION', 'NO', 'YES'];
    this.translate.get(keys).pipe(take(1)).subscribe(async value => {
      const alert = await this.alertController.create({
        header: value[keys[0]],
        message: value[keys[1]],
        buttons: [
          {
            text: value[keys[2]],
            role: 'cancel',
            cssClass: 'secondary'
          }, {
            text: value[keys[3]],
            handler: () => {
              this.loginService.logout();
            }
          }
        ]
      });
      alert.present();
    });
  }
}
