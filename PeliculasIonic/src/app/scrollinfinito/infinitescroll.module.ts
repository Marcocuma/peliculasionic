import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {InfinitescrollComponent} from './infinitescroll.component';
import {InfinitescrollRoutingModule} from './infinitescroll-routing.module';
import {MovieServiceService} from '../services/movie-service.service';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, InfinitescrollRoutingModule, HttpClientModule,
    HttpClientJsonpModule, TranslateModule],
  providers: [
    MovieServiceService
  ],
  declarations: [InfinitescrollComponent],
  exports: [InfinitescrollComponent]
})
export class InfinitescrollModule {}
