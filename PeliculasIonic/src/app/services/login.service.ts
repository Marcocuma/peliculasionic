import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {ToastController} from '@ionic/angular';
import {PersonalmoviesService} from './personalmovies.service';
import {UiService} from './ui.service';
import {take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public loggedIn = false;
  public user;
  constructor(public firebaseService: AngularFireAuth, public toastController: ToastController,
              public personalMovies: PersonalmoviesService, public uiService: UiService) {
    // Comprueba si hay un usuario logueado, y si esta entra directamente
    this.firebaseService.user.pipe(take(1)).subscribe(value => {
      if (value != null) {
        this.loggedIn = true;
        this.user = value;
        this.personalMovies.loadData(value.uid);
        this.uiService.getFromDatabase(value.uid);
      }
    });
  }
  register(email: string, pass: string){
      this.firebaseService.createUserWithEmailAndPassword(email,
        pass)
        .then(
          res => {
            if (res.user != null) {
              this.loggedIn = true;
              this.user = res.user;
              this.personalMovies.loadData(res.user.uid);
            }
          },
          error => {
            switch (error.code) {
              case 'auth/invalid-email': this.uiService.setToast('INVALIDEMAIL'); break;
              case 'auth/weak-password': this.uiService.setToast('WEAKPASS'); break;
              case 'auth/email-already-in-use': this.uiService.setToast('EMAILUSED'); break;
            }
          });
  }
  login(email: string, pass: string){
    this.firebaseService.signInWithEmailAndPassword(email,
      pass)
      .then(
        res => {
          if (res.user != null) {
            this.loggedIn = true;
            this.user = res.user;
            this.personalMovies.loadData(res.user.uid);
            this.uiService.getFromDatabase(res.user.uid);
          }
        },
        error => {
          switch (error.code) {
            case 'auth/invalid-email': this.uiService.setToast('INVALIDEMAIL'); break;
            case 'auth/wrong-password': this.uiService.setToast('WRONGPASS'); break;
            case 'auth/user-not-found': this.uiService.setToast('USERNOTFOUND'); break;
          }
        });
  }
  anonimousLogin(){
    // Pone la variable de acceso a la pagina a false y
    this.loggedIn = true;
    this.personalMovies.loadData('');
  }
  getUser(){
    return this.user;
  }
  logout(){
    // Se desloguea de firebase y limpia las variables de sesion
      if (this.firebaseService.currentUser){
        this.firebaseService.signOut().then(value => {
          this.loggedIn = false;
          this.user = null;
          this.personalMovies.clearSession();
          this.uiService.clearSession();
        });
      }
  }
}
