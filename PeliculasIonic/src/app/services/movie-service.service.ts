import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import {JsonObject} from '@angular/compiler-cli/ngcc/src/packages/entry_point';
import {MovieDetails} from '../models/movieDetails';
@Injectable({
  providedIn: 'root'
})
export class MovieServiceService {
  constructor(public http: HttpClient) {}
  // Estoy probando el tipo del observable para intentar obetener las lista de valores del JSON devuelto
  public getMovies(title: string): Observable<Response>{
    return this.http.get<Response>('https://www.omdbapi.com/?apikey=3695b132&s=' + title, {responseType: 'json'});
  }
  public getMovieDetail(id: string): Observable<object>{
    return this.http.get<object>('https://www.omdbapi.com/?apikey=3695b132&i=' + id + '&plot=full', {responseType: 'json'});
  }
  public getPagedMovies(title: string, page: number): Observable<Response>{
    return this.http.get<Response>('https://www.omdbapi.com/?apikey=3695b132&s=' + title + '&page=' + page, {responseType: 'json'});
  }
}
