import { TestBed } from '@angular/core/testing';

import { PersonalmoviesService } from './personalmovies.service';

describe('PersonalmoviesService', () => {
  let service: PersonalmoviesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonalmoviesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
