import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {SavedMovie} from '../models/savedMovie';
import {BehaviorSubject, Observable} from 'rxjs';
import { Storage } from '@ionic/storage';
import {AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class PersonalmoviesService implements OnDestroy{
  private movies: BehaviorSubject<Map<string, SavedMovie>>;
  private idUser: string;
  constructor(public storage: Storage, public database: AngularFireDatabase) {
    // Inicializo el objecto de escucha de las peliculas, y despues si hay guardadas en la base de datos
    // las cargo
    this.movies = new BehaviorSubject<Map<string, SavedMovie>>(new Map<string, SavedMovie>());
  }

  ngOnDestroy() {
    this.saveData();
  }
  loadData(idUser: string){
    // Si el id esta vacio, obtiene la lista de la base de datos local
    // En cambio si esta lleno, de firebase
    this.idUser = idUser;
    if (idUser.length === 0) {
      this.storage.create().then(() => {
        this.storage.get('movies').then(value => {
          if (value != null) {
            this.movies.next(value);
          }
        });
      });
    } else {
      this.database.database.ref('movies').child(idUser).get().then(value => {
        if (value != null){
         this.movies.next(new Map<string, SavedMovie>(value.val()));
        }
      });
    }
  }
  getMovies(): Observable<Map<string, SavedMovie>> {
    return this.movies.asObservable();
  }

  setMovies(value: Map<string, SavedMovie>){
    // Cambia el mapa del observable y guarda los datos
    this.movies.next(value);
    this.saveData();
  }

  addMovie(movie: SavedMovie, id: string){
    this.movies.next(this.movies.value.set(id, movie));
    this.saveData();
  }
  removeMovie(id: string){
    this.movies.value.delete(id);
    this.movies.next(this.movies.value);
  }
  exist(id: string){
    return this.movies.value.has(id);
  }
  async saveData() {
    // Si el idUser esta vacio, significa que esta logueado como anonimo por lo que guarda
    // la lista en la base de datos local, si esta lleno, lo guarda en la base de datos
    if (this.idUser === '') {
      return this.storage.set('movies', this.movies.value);
    } else {
      return this.database.database.ref('movies').child(this.idUser).set(Array.from(this.movies.value));
    }
  }
  clearSession(){
    // Guarda los datos y vacia el map
    this.saveData().then(value => {
      this.movies = new BehaviorSubject<Map<string, SavedMovie>>(new Map<string, SavedMovie>());
    });
  }
}
