import { Injectable } from '@angular/core';
import {ActionSheetController, ToastController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {TranslateService} from '@ngx-translate/core';
import {take} from 'rxjs/operators';
import {SavedMovie} from '../models/savedMovie';
import {AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  private idUser = '';
  constructor(public actionSheetController: ActionSheetController, public toastController: ToastController, public storage: Storage,
              public translate: TranslateService, public database: AngularFireDatabase) {
    // Si hay un tema guardado en el storage, lo aplica
    // Si no esta logueado lo coge del storage, si lo esta de firebase
      this.storage.create().then(() => {
        this.storage.get('color').then(value => {
          if (value != null) {
            this.changeTheme(value);
          }
        });
        this.storage.get('lang').then(value => {
          if (value != null) {
            this.changeLanguage(value);
          }
        });
      });
  }

  getFromDatabase(id: string){
    this.idUser = id;
    const keys: string[] = ['theme', 'lang'];
    this.database.database.ref('config').child(this.idUser).get().then(value => {
      if (value.val() != null){
        if ( value.val()[keys[1]] != null ){
          this.changeLanguage(value.val()[keys[1]]);
        }
        if ( value.val()[keys[0]] != null ){
          this.changeTheme(value.val()[keys[0]]);
        } else {
          this.changeTheme('');
        }
      }
    });
  }

  async openListColors() {
    // Despliega una lista con los temas.
    // Cuando se hace click en una, añade la clase al body
    const keys: string[] = ['THEME', 'LIGHT', 'DARK', 'ORANGE', 'GREEN', 'BLUE', 'CANCEL'];
    this.translate.get(keys).pipe(take(1)).subscribe(async value => {
      const actionSheet = await this.actionSheetController.create({
        header: value[keys[0]],
        cssClass: 'my-custom-class',
        buttons: [{
          text: value[keys[1]],
          role: 'destructive',
          handler: () => {
            this.changeTheme('');
          }
        }, {
          text: value[keys[2]],
          handler: () => {
            this.changeTheme('dark');
          }
        }, {
          text: value[keys[3]],
          handler: () => {
            this.changeTheme('yellow');
          }
        }, {
          text: value[keys[4]],
          handler: () => {
            this.changeTheme('green');
          }
        }, {
          text: value[keys[5]],
          handler: () => {
            this.changeTheme('blue');
          }
        }, {
          text: value[keys[6]],
          icon: 'close',
          role: 'cancel'
        }]
      });
      await actionSheet.present();
    });
  }
  async startLangList(){
    // Obtengo los valores de las keys que necesito y paso los valores y las keys a la lista
    // para que los incluya
    const keys: string[] = ['LANGUAGE', 'SPANISH', 'ENGLISH', 'CANCEL'];
    this.translate.get(keys).pipe(take(1)).subscribe(value => {
      this.openLangList(value, keys);
    });
  }
  private async openLangList(values: Map<string, string>, keys: string[]){
    // Obtiene los valores de los string con las keys
    // Abre la lista para seleccionar el idioma
    const langAction = await this.actionSheetController.create({
      header: values[keys[0]],
      cssClass: 'my-custom-class',
      buttons: [{
        text: values[keys[1]],
        handler: () => {
          this.changeLanguage('es');
        }
      }, {
        text: values[keys[2]],
        handler: () => {
          this.changeLanguage('en');
        }
      },  {
        text: values[keys[3]],
        icon: 'close',
        role: 'cancel'
      }]
    });
    await langAction.present();
  }
  private changeLanguage(lang: string){
    // Cambio el idioma y lo guardo para la proxima vez que entre
    this.translate.setDefaultLang(lang);
    this.storage.set('lang', lang);
    if ( this.idUser.length !== 0 ){
      this.database.database.ref('config').child(this.idUser).child('lang').set(lang);
    }
  }
  changeTheme(cssclass: string){
    // Elimina la etiqueta que este puesta (no sabemos cual estara exactamente asi que borramos todas)
    // Si el string de la clase no esta vacio, le añade la clase al body y la guarda en el storage
    // Si esta vacio lo elimina del storage (Tema por defecto)
    document.body.classList.remove('dark', 'yellow', 'blue', 'green');
    if (cssclass.length > 0) {
      document.body.classList.toggle(cssclass, true);
      this.storage.set('color', cssclass);
    } else {
      this.storage.remove('color');
    }
    if ( this.idUser.length !== 0 ){
      if (cssclass.length > 0) {
        this.database.database.ref('config').child(this.idUser).child('theme').set(cssclass);
      } else {
        this.database.database.ref('config').child(this.idUser).child('theme').remove();
      }
    }
  }
  clearSession(){
    this.idUser = '';
  }
  public async setToast(key: string) {
    this.translate.get(key).pipe(take(1)).subscribe(value => {
      this.openToast(value);
    });
  }
  public async openToast(messaje){
    const toast = await this.toastController.create({
      message: messaje,
      duration: 2000
    });
    await toast.present();
  }
}
