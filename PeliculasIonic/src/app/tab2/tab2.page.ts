import { Component } from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {MovieServiceService} from '../services/movie-service.service';
import {MovieDetails} from '../models/movieDetails';
import {Rating} from '../models/rating';
import {Movie} from '../models/movie';
import {SavedMovie} from '../models/savedMovie';
import {PersonalmoviesService} from '../services/personalmovies.service';
import {ActionSheetController, AlertController, ToastController} from '@ionic/angular';
import {LoginService} from '../services/login.service';
import {take} from 'rxjs/operators';
import {UiService} from '../services/ui.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  private id: string;
  movieDetail: MovieDetails;
  loaded: boolean;
  loading = false;
  constructor(private service: MovieServiceService, private router: Router, public personalMovie: PersonalmoviesService,
              public loginService: LoginService,
              public alertController: AlertController, public actionSheetController: ActionSheetController,
              public uiService: UiService, public translate: TranslateService) {
    this.loaded = false;
    this.router.events.subscribe((event) => {
      // Cuando el router redirige a esta pagina, compruebo que en los extras se ha pasado un id,
      // y si es asi le mando al servicio buscar la pelicula
      if (event instanceof NavigationEnd) {
        this.movieDetail = null;
        this.loaded = false;
        if (this.router.getCurrentNavigation().extras.state){
          this.id = this.router.getCurrentNavigation().extras.state.id;
          this.loading = true;
          this.service.getMovieDetail(this.id).pipe(take(1)).subscribe(
            data => {
              const camps: string[] = ['Title', 'Year', 'Rated', 'Released', 'Runtime',
              'Genre', 'Director', 'Writer', 'Actors', 'Plot', 'Language', 'Country',
              'Awards', 'Poster', 'Metascore', 'imdbRating', 'imdbVotes', 'imdbID',
              'Type', 'DVD', 'BoxOffice', 'Production', 'Website', 'Response'];
              const ratingsCamp = 'Ratings';
              const campsRating: string[] = ['Source', 'Value'];
              const ratingsList: Rating[] = [];
              for (const result of data[ratingsCamp]) {
                ratingsList.push(new Rating(result[campsRating[0]], result[campsRating[1]]));
              }
              this.movieDetail = new MovieDetails(data[camps[0]], data[camps[1]], data[camps[2]], data[camps[3]],
                data[camps[4]], data[camps[5]], data[camps[6]], data[camps[7]], data[camps[8]], data[camps[9]], data[camps[10]],
                data[camps[11]], data[camps[12]], data[camps[13]], ratingsList, data[camps[14]], data[camps[15]], data[camps[16]],
                data[camps[17]], data[camps[18]], data[camps[19]], data[camps[20]], data[camps[21]], data[camps[22]], data[camps[23]]);
              this.loaded = true;
              this.loading = false;
            }
          );
        }
      }
    });
  }
  openURL(url: string){
    // Abre el enlace en una nueva pestaña
    window.open(url, '_system');
  }
  public seeLater(movieDetails: MovieDetails){
    // Comprueba si existe la pelicula en la lista, y si no existe la añade
    const movie: Movie = new Movie(movieDetails.imdbID, movieDetails.title, movieDetails.year, movieDetails.poster);
    if (!this.personalMovie.exist(movie.id)){
      const movieSeeLater = new SavedMovie(movie, 1, false);
      this.personalMovie.addMovie(movieSeeLater, movie.id);
      this.uiService.setToast('MOVIEADDED');
    }
  }

  async logout() {
    const keys: string[] = ['LOGOUT', 'LOGOUTQUESTION', 'NO', 'YES'];
    this.translate.get(keys).pipe(take(1)).subscribe(async value => {
      const alert = await this.alertController.create({
        header: value[keys[0]],
        message: value[keys[1]],
        buttons: [
          {
            text: value[keys[2]],
            role: 'cancel',
            cssClass: 'secondary'
          }, {
            text: value[keys[3]],
            handler: () => {
              this.loginService.logout();
            }
          }
        ]
      });
      alert.present();
    });
  }

}

