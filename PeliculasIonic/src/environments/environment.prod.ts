export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC-sTQOoo367l9uKXo_aqNSYBy4xqczjQ8',
    authDomain: 'ionicmovies-f4e3f.firebaseapp.com',
    projectId: 'ionicmovies-f4e3f',
    databaseURL: 'https://ionicmovies-f4e3f-default-rtdb.europe-west1.firebasedatabase.app',
    storageBucket: 'ionicmovies-f4e3f.appspot.com',
    messagingSenderId: '323585096439',
    appId: '1:323585096439:web:97221ac5d553a4461631f2'
  }
};
